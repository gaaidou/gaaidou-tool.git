package cn.gaaidou.common.utils;

/**
 * @author Martin
 * @date 2021/6/29 11:07
 */
public class StringUtil {
    public static boolean isEmpty(String str) {
        return null == str || "".equals(str.trim());
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
}
