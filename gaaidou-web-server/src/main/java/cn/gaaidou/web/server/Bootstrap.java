package cn.gaaidou.web.server;


import cn.gaaidou.web.server.socket.BioServer;

/**
 * @author Martin
 * @date 2021/5/8 17:53
 */
public class Bootstrap {
    public static void main(String[] args) {
        BioServer server = new BioServer();
        server.start();
    }
}
