package cn.gaaidou.web.server.socket;

import cn.gaaidou.web.server.serivce.CustomService;
import cn.gaaidou.web.server.util.AsyncUtil;
import org.apache.log4j.Logger;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Martin
 * @date 2021/6/29 11:11
 */
public class BioServer {

    private static Logger logger = Logger.getLogger(BioServer.class);

    private int port;

    public BioServer() {
        this.port = 8080;
    }

    public BioServer(int port) {
        this.port = port;
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                // Socket 阻塞
                Socket accept = serverSocket.accept();
                AsyncUtil.exec(() -> CustomService.handleHttp(accept));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


}
