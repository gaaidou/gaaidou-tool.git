package cn.gaaidou.web.server.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Martin
 * @date 2021/6/29 15:47
 */
public class AsyncThreadFactory implements ThreadFactory {

    private final AtomicInteger index = new AtomicInteger(1);

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, String.format("THREAD-POOL-%s", index.getAndIncrement()));
    }
}
