package cn.gaaidou.web.server.http;

import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author Martin
 * @date 2021/6/29 11:43
 */
@NoArgsConstructor
public class HttpHeader extends HashMap<String, List<String>> {

    public HttpHeader(List<String> httpLines) {
        for (int index = 1; index < httpLines.size() - 1; index++) {
            String value = httpLines.get(index);
            String[] split = value.split(":");
            if (split.length == 2) {
                this.put(split[0].trim(), Arrays.asList(split[1].trim().split(",")));
            }
        }
    }
}
