package cn.gaaidou.web.server.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Martin
 * @date 2021/6/29 15:04
 */
public class AsyncUtil {

    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 100, 60, TimeUnit.MINUTES, new LinkedBlockingQueue<>(),
            new AsyncThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());


    public static void exec(AsyncEvent event) {
        threadPoolExecutor.submit(event::apply);
    }


    @FunctionalInterface
    public interface AsyncEvent {
        /**
         * 执行本方法
         */
        void apply();
    }
}
