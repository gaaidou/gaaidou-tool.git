package cn.gaaidou.web.server.serivce;

import cn.gaaidou.common.utils.IOUtil;
import cn.gaaidou.web.server.http.HttpHeader;
import cn.gaaidou.web.server.http.HttpRequest;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

/**
 * @author Martin
 * @date 2021/7/1 11:29
 */
public class CustomService {
    public static void handleHttp(Socket accept) {
        try {
            List<String> httpLines = IOUtil.readSocketLines(accept.getInputStream());
            HttpRequest request = new HttpRequest(new HttpHeader(httpLines), httpLines.get(httpLines.size() - 1));

            OutputStream outputStream = accept.getOutputStream();
            PrintWriter printWriter = IOUtil.buildPrint(outputStream);
            // 响应头的HTTP一定要大写, 否则会解析错误
            printWriter.println("HTTP/1.1 200 OK");
            printWriter.println("Content-Type: application/json");
            printWriter.println();
            printWriter.println("{a:12, bbb:1333}");
            printWriter.flush();
            printWriter.close();
            accept.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
