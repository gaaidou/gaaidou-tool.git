package cn.gaaidou.web.server.socket;

import cn.gaaidou.web.server.serivce.CustomService;
import cn.gaaidou.web.server.util.AsyncUtil;
import org.apache.log4j.Logger;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * NIO适用于多条连接、多次请求，即需要多个线程多次操作，使用NIO可以减少创建线程和线程频繁切换的消耗；
 * 传统IO适用于少量用户，每次请求需要传输大量数据，但是请求次数少，使用传统IO可以提高传输的效率。
 * （这里所说的NIO代指非阻塞IO，传统IO代表阻塞IO）
 * @author Martin
 * @date 2021/6/29 11:11
 */
public class NioServer {

    private static Logger logger = Logger.getLogger(NioServer.class);

    private int port;

    public NioServer() {
        this.port = 8080;
    }

    public NioServer(int port) {
        this.port = port;
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                // Socket 阻塞
                Socket accept = serverSocket.accept();
                AsyncUtil.exec(() -> CustomService.handleHttp(accept));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


}
