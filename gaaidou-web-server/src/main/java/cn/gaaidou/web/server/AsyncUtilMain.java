package cn.gaaidou.web.server;

import cn.gaaidou.web.server.util.AsyncThreadFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Martin
 * @date 2021/6/29 15:09
 */
public class AsyncUtilMain {

    /**
     * 核心线程数
     */
    private static final int CORE_POOL_SIZE = 5;
    /**
     * 最大线程数
     */
    private static final int MAX_POOL_SIZE = 10;
    /**
     * 阻塞队列长度
     */
    private static final int QUEUE_SIZE = 3;
    /**
     * 需求线程数
     */
    private static final int REQUIREMENT_POOL_SIZE = 15;


    public static void main(String[] args) {

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, 60, TimeUnit.MINUTES, new LinkedBlockingQueue<>(QUEUE_SIZE),
                new AsyncThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

        for (int index = 0; index < REQUIREMENT_POOL_SIZE; index++) {
            int value = index;
            threadPoolExecutor.submit(() -> System.out.println(Thread.currentThread().getName() + ":" + value));
        }

        threadPoolExecutor.shutdown();
    }
}
