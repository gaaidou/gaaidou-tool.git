package cn.gaaidou.web.server.http;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Martin
 * @date 2021/6/29 11:43
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class HttpRequest {
    /**
     * Http请求头
     */
    private HttpHeader header;

    /**
     * Http请求Body
     */
    private String body;

}
