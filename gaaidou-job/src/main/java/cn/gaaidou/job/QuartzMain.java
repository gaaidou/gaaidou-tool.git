package cn.gaaidou.job;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

/**
 * 1、任务定义机制
 *
 * 2、运行超时
 *
 * 3、出现异常的补偿
 *
 * 4、分片需求
 *
 * 5、线程池 线程总数、名称
 *
 * @author Martin
 * @date 2021/7/1 15:30
 */
public class QuartzMain {
    public static void main(String[] args) throws Exception {
        // 1、建立Scheduler
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        Date now = new Date();

        scheduler.scheduleJob(
                JobBuilder
                        .newJob(DeployJob.class)
                        .withIdentity("DeployJob", "group")
                        .build(),
                TriggerBuilder.newTrigger()
                        .startAt(now)
                        .withIdentity("trigger-DeployJob", "trigger-group")
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                // 相隔多少秒活执行(只精确到时分秒, 不精确到年月日)
                                .withIntervalInHours(10)
                                // 如果不加本项, 则后期不会重复运行
                                .repeatForever())
                        .build());

        scheduler.scheduleJob(
                JobBuilder
                        .newJob(CronJob.class)
                        .withIdentity("CronJob", "group")
                        .build(),
                TriggerBuilder.newTrigger()
                        .startAt(now)
                        .withIdentity("trigger-CronJob", "trigger-group")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0/20 * * * * ?"))
                        .build());

        scheduler.scheduleJob(
                JobBuilder
                        .newJob(DailyTimeIntervalJob.class)
                        .withIdentity("DailyTimeIntervalJob", "group")
                        .build(),
                TriggerBuilder.newTrigger()
                        .startAt(now)
                        .withIdentity("trigger-DailyTimeIntervalJob", "trigger-group")
                        .withSchedule(DailyTimeIntervalScheduleBuilder.dailyTimeIntervalSchedule()
                                .withInterval(30, DateBuilder.IntervalUnit.SECOND))
                        .build());

        scheduler.scheduleJob(
                JobBuilder
                        .newJob(CalendarIntervalJob.class)
                        .withIdentity("CalendarIntervalJob", "group")
                        .build(),
                TriggerBuilder.newTrigger()
                        .startAt(now)
                        .withIdentity("trigger-CalendarIntervalJob", "trigger-group")
                        .withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule()
                                .withIntervalInSeconds(40))
                        .build());
        scheduler.start();

    }
}
