package cn.gaaidou.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Martin
 * @date 2021/7/1 19:42
 */
public class DailyTimeIntervalJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(Thread.currentThread().getName() + ">>>>>>>>>>>>>DailyTimeIntervalJob>>>>>>>>>> 当前时间: " + sdf.format(new Date()));
    }
}
