package cn.gaaidou.rpc.client;

import cn.gaaidou.rpc.api.service.StudentService;
import cn.gaaidou.rpc.api.vo.StudentVo;
import cn.gaaidou.rpc.framework.RpcFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/3 12:48
 */
@Slf4j
public class ClientMain {
    public static void main(String[] args) {
        /*
         * 1、从Factory创建一个代理对象, 如果是Spring框架中可以处理为单例, 然后注入即可使用
         */
        StudentService studentService = RpcFactory.newInstance(StudentService.class);

        // 2、构造请求参数
        StudentVo studentVo = new StudentVo();
        studentVo.setId(1L);
        studentVo.setAddress("广东省深圳市");
        studentVo.setCardNo("5201314");
        studentVo.setName("Martin");
        studentVo.setSex(1);

        // 3、远程调用
        int result = studentService.save(studentVo);

        // 4、根据RPC返回结果进行处理
        log.info("服务端响应Code: {}", result);
    }
}
