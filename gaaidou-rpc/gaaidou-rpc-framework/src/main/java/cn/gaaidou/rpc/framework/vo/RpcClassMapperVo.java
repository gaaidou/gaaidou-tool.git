package cn.gaaidou.rpc.framework.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Martin
 * @date 2021/7/5 11:18
 */
@Setter
@Getter
@AllArgsConstructor
public class RpcClassMapperVo implements Serializable {
    private Class<?> clazz;

    /**
     * 请求参数
     */
    private byte[] data;
}
