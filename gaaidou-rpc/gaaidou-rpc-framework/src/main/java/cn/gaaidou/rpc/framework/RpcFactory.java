package cn.gaaidou.rpc.framework;

import java.lang.reflect.Proxy;

/**
 * @author Martin
 * @date 2021/7/3 12:47
 */
public class RpcFactory {

    /**
     * 创建代理对象
     *
     * @param clazz
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(Class<T> clazz) {
        return (T) Proxy.newProxyInstance(RpcFactory.class.getClassLoader(), new Class<?>[]{clazz}, new RpcClientProxy());
    }
}
