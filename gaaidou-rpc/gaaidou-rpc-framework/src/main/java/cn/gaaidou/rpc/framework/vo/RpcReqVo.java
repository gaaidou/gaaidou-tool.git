package cn.gaaidou.rpc.framework.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Martin
 * @date 2021/7/5 11:18
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RpcReqVo implements Serializable {
    /**
     * 接口类
     */
    private String clazzName;

    /**
     * 接口方法类
     */
    private String methodName;

    /**
     * 请求参数
     */
    private List<RpcClassMapperVo> reqInfos;
}
