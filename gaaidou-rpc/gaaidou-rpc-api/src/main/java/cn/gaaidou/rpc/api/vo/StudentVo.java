package cn.gaaidou.rpc.api.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Martin
 * @date 2021/7/3 12:41
 */
@Setter
@Getter
public class StudentVo implements Serializable {

    private Long id;

    private String name;

    private String cardNo;

    private int sex;

    private String address;


}
