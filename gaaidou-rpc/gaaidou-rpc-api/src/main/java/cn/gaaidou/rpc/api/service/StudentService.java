package cn.gaaidou.rpc.api.service;

import cn.gaaidou.rpc.api.vo.StudentVo;

/**
 * @author Martin
 * @date 2021/7/3 12:44
 */
public interface StudentService {

    /**
     * 保存学生信息
     *
     * @param student 学生信息
     * @return 状态码
     */
    int save(StudentVo student);
}
