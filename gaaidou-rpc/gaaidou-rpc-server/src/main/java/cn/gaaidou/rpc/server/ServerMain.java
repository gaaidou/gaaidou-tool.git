package cn.gaaidou.rpc.server;

import cn.gaaidou.rpc.framework.RpcServer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author Martin
 * @date 2021/7/3 13:04
 */
@Slf4j
public class ServerMain {
    public static void main(String[] args) {
        /*
         * 创建RPC服务端
         */
        try {
            RpcServer rpcServer = new RpcServer();
            rpcServer.start();
        } catch (IOException e) {
            log.error("Rpc服务端异常, 异常原因: {}", e.getMessage(), e);
        }

    }
}
