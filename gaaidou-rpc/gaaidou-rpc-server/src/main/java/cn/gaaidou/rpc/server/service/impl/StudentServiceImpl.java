package cn.gaaidou.rpc.server.service.impl;

import cn.gaaidou.rpc.api.service.StudentService;
import cn.gaaidou.rpc.api.vo.StudentVo;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/3 13:06
 */
@Slf4j
public class StudentServiceImpl implements StudentService {
    @Override
    public int save(StudentVo student) {

        // 保存学生信息, 本例用打印来代替
        log.info("服务端获取学生信息: [姓名: {} 学号: {} 地址: {}] ", student.getName(), student.getCardNo(), student.getAddress());

        // 返回成功的code
        return 200;
    }
}
