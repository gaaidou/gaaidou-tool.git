import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.DefaultTempFileCreationStrategy;
import org.apache.poi.util.TempFile;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author Martin
 * @date 2021/7/14 12:19
 */
public class MainTest {
    public static void main(String[] args) throws Exception {
        Long start = System.currentTimeMillis();
        //内存最大存放100行数据 超过100自动刷新到硬盘中
        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
        TempFile.setTempFileCreationStrategy(new DefaultTempFileCreationStrategy(new File("f:/temp/temp")));
        wb.setCompressTempFiles(true);
        Sheet sh = wb.createSheet();
        for(int rownum = 0; rownum < 500000; rownum++){
            Row row = sh.createRow(rownum);//一行
            for(int cellnum = 0; cellnum < 10; cellnum++){
                Cell cell = row.createCell(cellnum); //一行中一个方格
                cell.setCellValue("测试");
            }

        }

        FileOutputStream out = new FileOutputStream("f:/temp/sxssf.xlsx");
        wb.write(out);
        out.close();


        // dispose of temporary files backing this workbook on disk
        //处理在磁盘上支持本工作簿的临时文件
        wb.dispose();
        wb.close();
        Long end = System.currentTimeMillis();
        System.out.println(end - start + "ms"); //50万条数据写入大概在16秒
    }
}
