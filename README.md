# Gaaidou学习框架 - 不造轮子, 但看轮子

技术栈：

    Java8   整个学习框架以Java8为基础, 尽可能少引入一些高层次的框架
    Reflections Java反射框架
    Lombok lombok框架(免Getter And Setter)
    
模块划分：

    gaaidou-common: 工具类、核心类模块
    gaaidou-job: 定时任务模块
    gaaidou-rpc: RPC框架模块
    --  gaaidou-rpc-api:  接口定义(测试)
    --  gaaidou-rpc-client:  客户端(测试)
    --  gaaidou-rpc-server: 服务端(测试)
    --  gaaidou-rpc-framework: RPC底层框架
    gaaidou-web-server: 任务跑批层, 使用Elastic-Job进行分布式任务跑批
    