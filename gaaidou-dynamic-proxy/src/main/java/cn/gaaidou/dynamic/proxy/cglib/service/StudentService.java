package cn.gaaidou.dynamic.proxy.cglib.service;

import cn.gaaidou.dynamic.proxy.cglib.annotation.SystemLog;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/13 19:33
 */
@Slf4j
public class StudentService {


    /**
     * 普通service方法
     *
     * @param teacherName
     * @return
     */
    @SystemLog
    public int learn(String teacherName) {
        log.info("{}老师的课上得真好", teacherName);
        return 1;
    }

    /**
     * final修饰的方法
     * 虽然有SystemLog注解, 但因为子类不能继承, 因此也不会有动态代理效果
     */
    @SystemLog
    public final void finalMethod() {
        log.info("本方法不会生成代理子类");

    }

    public int sleep(String teacherName) {
        log.info("{}老师的课太无聊了我要睡觉了", teacherName);
        return 1;
    }
}
