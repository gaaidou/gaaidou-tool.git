package cn.gaaidou.dynamic.proxy.cglib;

import cn.gaaidou.dynamic.proxy.cglib.service.StudentService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/13 19:33
 */
@Slf4j
public class CglibTest {
    public static void main(String[] args) {
        // 1、创建对象
        StudentService studentService = BeanFactory.newInstance(StudentService.class);

        // 2、执行方法
        log.info("StudentService实际执行类: {}", studentService.toString());
        int learnResult = studentService.learn("Martin");
        log.info("learn方法返回参数: {}", learnResult);
        int sleepResult = studentService.sleep("Martin");
        log.info("sleep方法返回参数: {}", sleepResult);
        studentService.finalMethod();
    }
}
