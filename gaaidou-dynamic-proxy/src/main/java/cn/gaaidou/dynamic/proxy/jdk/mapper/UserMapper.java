package cn.gaaidou.dynamic.proxy.jdk.mapper;

import cn.gaaidou.dynamic.proxy.jdk.annotation.Params;
import cn.gaaidou.dynamic.proxy.jdk.annotation.Select;
import cn.gaaidou.dynamic.proxy.jdk.entity.User;

/**
 * @author Martin
 * @date 2021/7/14 19:05
 */
public interface UserMapper {

    /**
     * 通过Id查询用户对象
     *
     * @param id 用户id
     * @return
     */
    @Select("SELECT id, username, password FROM t_sys_user WHERE id = #{id}")
    User selectById(@Params("id") Long id);
}
