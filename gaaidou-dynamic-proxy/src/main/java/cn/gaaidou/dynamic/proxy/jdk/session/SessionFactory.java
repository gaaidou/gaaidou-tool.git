package cn.gaaidou.dynamic.proxy.jdk.session;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/14 19:21
 */
@Slf4j
public class SessionFactory {

    /**
     * 开启一个session
     *
     * @return
     */
    public static SqlSession openSession() {
        log.info("加载资源信息, 初始化数据源, 加载xml mapper文件到内存等...");
        return new SqlSession();
    }
}
