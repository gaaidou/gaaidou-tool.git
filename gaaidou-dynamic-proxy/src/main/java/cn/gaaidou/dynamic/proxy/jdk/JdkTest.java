package cn.gaaidou.dynamic.proxy.jdk;

import cn.gaaidou.dynamic.proxy.jdk.entity.User;
import cn.gaaidou.dynamic.proxy.jdk.mapper.UserMapper;
import cn.gaaidou.dynamic.proxy.jdk.session.SessionFactory;
import cn.gaaidou.dynamic.proxy.jdk.session.SqlSession;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Martin
 * @date 2021/7/14 19:23
 */
@Slf4j
public class JdkTest {
    public static void main(String[] args) {
        // 1、开启一个Session
        SqlSession sqlSession = SessionFactory.openSession();
        // 2、从session中获取一个mapper对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        // 3、调用Mapper方法(模拟Mybatis, 不查询数据库, 直接返回)
        User user = userMapper.selectById(1L);
        // 4、处理结果
        log.info("查询的用户信息, id: {}, 姓名: {}, 密码: {}", user.getId(), user.getUsername(), user.getPassword());
    }
}
