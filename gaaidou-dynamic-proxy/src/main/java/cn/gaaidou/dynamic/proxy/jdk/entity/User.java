package cn.gaaidou.dynamic.proxy.jdk.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Martin
 * @date 2021/7/14 19:05
 */
@Setter
@Getter
public class User {
    /**
     * 用户Id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}
